export const blue = {
  '--primary' : '#3898FF',
  '--primary-variant': '#171717',

  '--secondary': '#3898FF',
  '--secondary-variant': '#3898FF',

  '--background': '#000',
  '--surface': '#171717',
  '--dialog': '#171717',
  '--cancel': '#BF0D0D',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#212121',
  '--on-secondary': '#212121',
  '--on-background': '#fff',
  '--on-surface': '#fff',
  '--on-cancel': '#fff',

 /* Colors were changed! The names do not represent the acual color values anymore! */
 
 '--green': '#499eef',
 '--red': '#BF0D0D',
 '--yellow': '#FFD54F',
 '--blue': '#3f51b5',
 '--purple': '#9c27b0',
 '--light-green': '#80ba24',
 '--grey': '#BDBDBD',
 '--grey-light': '#EEEEEE',
 '--black': '#212121',
 '--moderator': '#171717'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Black Theme',
      'de': 'Schwarz Theme'
    },
    'description': {
      'en': 'Ultra power saving theme',
      'de': 'Sehr Energiefreundliches Theme'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'primary'
};
